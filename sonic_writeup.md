# Sonic -- TUCTF2019

The challenge is a Caesar Cipher that doesn't limit your attempts, so you can send all possibilities until it gives the flag.

Flag: `TUCTF{W04H_DUD3_S0_F4ST_S0N1C_4PPR0V3S}`
